# raspberry-pi-dolphin-emu


Screenshot:

![](medias/1640521216-4-dolphin-5-raspberry-pi4-v4.png)






## Getting started

 

   dolphin-emu 5.0 on Raspberry PI RPI4




tested on the 4GB:

Machine model: Raspberry Pi 4 Model B Rev 1.4



Linux version 5.14.0-4-arm64 

Memory: 725516K/3866624K available






Recommended, RPI4 B with 4GB : 

![](medias/renkforce-rpi4-4GB.jpg)




## Installation

````

1.) Download Debian Image for SD/MMC


wget -c --no-check-certificate   "https://raspi.debian.net/tested/20211124_raspi_4_bookworm.img.xz"

2.) debootstrap

debootstrap --no-check-gpg sid . http://ftp.debian.org/debian/ 


3.) copy firmware and modules on the SD card with the new system.

4.) apt-get install dolphin-emu blackbox xinit xterm 
````

## Alternative, Compilation from Source

Ideally, it is to compile from source:

More infos. at :  
https://forums.raspberrypi.com/viewtopic.php?p=1931000&hilit=dolphin#p1930903

